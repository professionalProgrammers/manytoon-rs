use std::{
    fs::File,
    path::{
        Path,
        PathBuf,
    },
};

#[derive(argh::FromArgs)]
#[argh(description = "A CLI utility to download from manytoon")]
pub struct Command {
    #[argh(subcommand)]
    subcommand: Subcommand,
}

#[derive(argh::FromArgs)]
#[argh(subcommand)]
pub enum Subcommand {
    Search(SearchCommand),
    Download(DownloadCommand),
}

#[derive(argh::FromArgs)]
#[argh(subcommand, name = "search", description = "Search for a manga")]
pub struct SearchCommand {
    #[argh(positional, description = "the query search parameter")]
    pub query: String,
}

fn default_out_dir() -> PathBuf {
    ".".into()
}

#[derive(argh::FromArgs)]
#[argh(subcommand, name = "download", description = "Download a manga")]
pub struct DownloadCommand {
    #[argh(positional, description = "a url of the manga to download")]
    pub url: String,

    #[argh(
        option,
        short = 'o',
        description = "the dir to save the manga",
        default = "default_out_dir()"
    )]
    pub out_dir: PathBuf,
}

fn main() {
    let cmd: Command = argh::from_env();

    let mut tokio_rt = match tokio::runtime::Builder::new()
        .enable_all()
        .threaded_scheduler()
        .build()
    {
        Ok(rt) => rt,
        Err(e) => {
            eprintln!("Failed to create tokio runtime: {}", e);
            return;
        }
    };
    let client = manytoon::Client::new();

    match cmd.subcommand {
        Subcommand::Search(cmd) => {
            let search_result = match tokio_rt.block_on(client.search(&cmd.query)) {
                Ok(result) => result,
                Err(e) => {
                    eprintln!("Failed to search for '{}': {}", cmd.query, e);
                    return;
                }
            };

            println!("Search Results for '{}':", cmd.query);
            if search_result.results.is_empty() {
                println!("No Results");
            } else {
                for (i, entry) in search_result.results.iter().enumerate() {
                    println!("#{}", i + 1);
                    println!("Name: {}", entry.name);
                    println!("Link: {}", entry.link.as_str());
                    println!();
                }
            }
        }
        Subcommand::Download(cmd) => {
            let url = match manytoon::MangaUrl::from_url_str(&cmd.url) {
                Ok(url) => url,
                Err(e) => {
                    eprintln!("'{}' is not a valid MangaUrl ({})", cmd.url, e);
                    eprintln!("Using search...");

                    let mut search_result = match tokio_rt.block_on(client.search(&cmd.url)) {
                        Ok(result) => result,
                        Err(e) => {
                            eprintln!("Failed to search for '{}': {}", cmd.url, e);
                            return;
                        }
                    };

                    if search_result.results.is_empty() {
                        eprintln!("No Search Results.");
                    }

                    let url = search_result.results.swap_remove(0).link;
                    eprintln!("Using url '{}'", url.as_str());

                    url
                }
            };

            let manga = match tokio_rt.block_on(client.get_manga(&url)) {
                Ok(manga) => manga,
                Err(e) => {
                    eprintln!("Failed to get manga: {}", e);
                    return;
                }
            };

            println!();
            println!();
            println!("{}", manga.name);
            println!();
            for entry in manga.chapters.iter() {
                println!("Name: {}", entry.name);
                println!("Link: {}", entry.link.as_str());
                println!("Release Date: {}", entry.release_date);
                println!();
            }
            println!();

            let num_chapters = manga.chapters.len();
            let out_dir = cmd.out_dir.join(url.manga_slug());

            println!("Found {} chapters", num_chapters);
            println!("Downloading to '{}'", out_dir.display());
            println!();

            if let Err(e) = std::fs::create_dir_all(&out_dir) {
                eprintln!("Failed to create out dir: {}", e);
                return;
            }

            let num_chapters_digits = get_num_digits(num_chapters);
            for (i, chapter) in manga.chapters.iter().enumerate() {
                let out_dir = out_dir.join(chapter.link.manga_page_slug());
                if let Err(e) = std::fs::create_dir_all(&out_dir) {
                    eprintln!("Failed to create out dir: {}", e);
                    continue;
                }

                let i_digits = get_num_digits(i + 1);
                let padded_i = format!("{}{}", "0".repeat(num_chapters_digits - i_digits), i + 1);

                println!(
                    "[{}/{}] Downloading '{}'...",
                    padded_i, num_chapters, chapter.name
                );

                let page = match tokio_rt.block_on(client.get_manga_page(&chapter.link)) {
                    Ok(page) => page,
                    Err(e) => {
                        eprintln!("Failed to get chapter page {}: {}", i + 1, e);
                        continue;
                    }
                };

                let num_images = page.images.len();
                let num_images_digits = get_num_digits(num_images);
                for (j, image) in page.images.iter().enumerate() {
                    let j_digits = get_num_digits(j + 1);
                    let padded_j = format!("{}{}", "0".repeat(num_images_digits - j_digits), j + 1);

                    println!("    Downloading image {}/{}...", padded_j, num_images);
                    let file_name = match image.path_segments().and_then(|path| path.last()) {
                        Some(file_name) => file_name,
                        None => {
                            eprintln!("    Failed to locate filename from image url '{}'", image);
                            continue;
                        }
                    };

                    let file_ext = match Path::new(file_name)
                        .extension()
                        .map(|ext| ext.to_str().expect("UTF8 url"))
                    {
                        Some(ext) => ext,
                        None => {
                            eprintln!(
                                " Failed to locate filename extension from filename '{}'",
                                file_name
                            );
                            continue;
                        }
                    };

                    let file_name = format!("{}.{}", j + 1, file_ext);

                    let file = match File::create(out_dir.join(file_name)) {
                        Ok(file) => file,
                        Err(e) => {
                            eprintln!("    Failed to open file: {}", e);
                            continue;
                        }
                    };

                    if let Err(e) = tokio_rt.block_on(client.copy_res_to(image, file)) {
                        eprintln!("    Failed to save image: {}", e);
                        continue;
                    }
                }

                println!("Done.");
            }
        }
    }
}

fn get_num_digits(mut n: usize) -> usize {
    if n == 0 {
        return 1;
    }

    let mut digits = 0;

    while n != 0 {
        digits += 1;
        n /= 10;
    }

    digits
}
