mod client;
/// Api Types
pub mod types;

pub use crate::{
    client::Client,
    types::{
        Manga,
        MangaPage,
        MangaPageUrl,
        MangaUrl,
        SearchEntry,
        SearchResults,
    },
};
pub use url::Url;

const COMIC_URL_BASE: &str = "https://manytoon.com/comic";

/// Result Type
pub type Result<T> = std::result::Result<T, Error>;

/// Error Type
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// A Reqwest HTTP Error
    #[error("{0}")]
    Reqwest(#[from] reqwest::Error),

    /// Invalid URL
    #[error("{0}")]
    InvalidUrl(#[from] url::ParseError),

    /// Invalid HTTP Status
    #[error("{0}")]
    InvalidStatus(reqwest::StatusCode),

    /// Missing HTML Element
    #[error("missing HTML element '{0}'")]
    MissingElement(&'static str),

    /// Invalid HTML Element Link
    #[error("invalid link while parsing HTML '{0}'")]
    InvalidHtmlLink(url::ParseError),

    /// Invalid Manga Url
    #[error("invalid manga url '{0}'")]
    InvalidMangaUrl(Url),

    /// Invalid Manga Chapter Url
    #[error("invalid manga page url '{0}'")]
    InvalidMangaPageUrl(Url),

    /// Io Error
    #[error("{0}")]
    Io(#[from] std::io::Error),
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn invalid_search() {
        let client = Client::new();
        let search_result = client
            .search("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
            .await
            .unwrap();
        assert!(search_result.results.is_empty());
    }

    #[tokio::test]
    async fn it_works() {
        let client = Client::new();
        let search_result = client.search("close").await.unwrap();
        let first = &search_result.results[0];
        dbg!(&first);
        let manga = client.get_manga(&first.link).await.unwrap();
        dbg!(&manga);

        for chapter in manga.chapters.iter().take(2) {
            let chapter = client.get_manga_page(&chapter.link).await.unwrap();
            dbg!(chapter);
        }
    }
}
