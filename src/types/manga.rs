use crate::{
    types::MangaPageUrl,
    Error,
    Result,
};
use select::{
    document::Document,
    node::Node,
    predicate::{
        Class,
        Name,
        Text,
    },
};
use url::Url;

/// A Manga
#[derive(Debug)]
pub struct Manga {
    /// Manga name
    pub name: String,

    /// Chapters Data
    pub chapters: Vec<MangaChapter>,
}

impl Manga {
    /// Try to make a Manga from a Document
    pub(crate) fn from_doc(doc: &Document) -> Result<Self> {
        let mut chapters = doc
            .find(Class("wp-manga-chapter"))
            .map(MangaChapter::from_node)
            .collect::<Result<Vec<_>>>()?;

        chapters.reverse();

        let name = doc
            .find(Class("post-title"))
            .next()
            .ok_or(Error::MissingElement(".post-title"))?
            .find(Name("h3"))
            .next()
            .ok_or(Error::MissingElement(".post-title > h3"))?
            .find(Text)
            .last()
            .and_then(|text| text.as_text())
            .ok_or(Error::MissingElement(".post-title > h3 > text"))?
            .trim()
            .to_string();

        Ok(Self { name, chapters })
    }
}

/// A manga chapter. Only hold data. Get a MangaPage to access images.
#[derive(Debug)]
pub struct MangaChapter {
    /// Chapter name
    pub name: String,

    /// Chapter Page link
    pub link: MangaPageUrl,

    /// Chapter release date
    pub release_date: String,
}

impl MangaChapter {
    /// Try to make a MangaChapter from a Document
    pub(crate) fn from_node(el: Node) -> Result<Self> {
        let link_el = el
            .find(Name("a"))
            .next()
            .ok_or(Error::MissingElement("a"))?;

        let name = link_el
            .find(Text)
            .next()
            .and_then(|text| text.as_text())
            .ok_or(Error::MissingElement("a > text"))?
            .trim()
            .to_string();

        let link = MangaPageUrl::from_url(
            Url::parse(
                link_el
                    .attr("href")
                    .ok_or(Error::MissingElement("a[href]"))?,
            )
            .map_err(Error::InvalidHtmlLink)?,
        )
        .map_err(Error::InvalidMangaPageUrl)?;

        let release_date = el
            .find(Class("chapter-release-date"))
            .next()
            .ok_or(Error::MissingElement(".chapter-release-date"))?
            .find(Name("i"))
            .next()
            .ok_or(Error::MissingElement(".chapter-release-date > i"))?
            .find(Text)
            .next()
            .and_then(|text| text.as_text())
            .ok_or(Error::MissingElement(".chapter-release-date > i > text"))?
            .to_string();

        Ok(Self {
            name,
            link,
            release_date,
        })
    }
}

/// A Page of images. Represents the "data" portion of a MangaChapter.
#[derive(Debug)]
pub struct MangaPage {
    /// Image Urls of this page
    pub images: Vec<Url>,
}

impl MangaPage {
    /// Try to make a MangaPage form a Document
    pub(crate) fn from_doc(doc: &Document) -> Result<Self> {
        let images = doc
            .find(Class("reading-content"))
            .next()
            .ok_or(Error::MissingElement(".reading-content"))?
            .find(Name("img"))
            .map(|el| {
                Url::parse(
                    el.attr("src")
                        .ok_or(Error::MissingElement(".reading-content > img[src]"))?
                        .trim(),
                )
                .map_err(Error::InvalidHtmlLink)
            })
            .collect::<Result<Vec<_>>>()?;

        Ok(Self { images })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const MANGA: &str = include_str!("../../test_data/manga.html");

    #[test]
    fn parse_manga() {
        let doc = Document::from(MANGA);
        let manga = Manga::from_doc(&doc).unwrap();
        dbg!(&manga);
    }
}
