use crate::{
    types::MangaUrl,
    Error,
    Result,
};
use select::{
    document::Document,
    node::Node,
    predicate::{
        Class,
        Name,
        Text,
    },
};
use url::Url;

/// Results for a search
#[derive(Debug)]
pub struct SearchResults {
    /// The search results
    pub results: Vec<SearchEntry>,
}

impl SearchResults {
    /// Make a SearchResults from a Document
    pub(crate) fn from_doc(doc: &Document) -> Result<Self> {
        let results = if doc.find(Class("no-results")).next().is_some() {
            Vec::new()
        } else {
            doc.find(Class("search-wrap"))
                .next()
                .ok_or(Error::MissingElement(".search-wrap"))?
                .find(Class("c-tabs-item"))
                .next()
                .ok_or(Error::MissingElement(".search-wrap > .c-tabs-item"))?
                .find(Class("c-tabs-item__content"))
                .map(SearchEntry::from_node)
                .collect::<Result<Vec<_>>>()?
        };

        Ok(Self { results })
    }
}

/// A Search Entry
#[derive(Debug)]
pub struct SearchEntry {
    /// Entry Name
    pub name: String,

    /// The link of the manga
    pub link: MangaUrl,
}

impl SearchEntry {
    /// Make a SearchEntry from a Node
    pub(crate) fn from_node(el: Node) -> Result<Self> {
        let title_link = el
            .find(Class("post-title"))
            .next()
            .ok_or(Error::MissingElement(".post-title"))?
            .find(Name("a"))
            .next()
            .ok_or(Error::MissingElement(".post-title > a"))?;

        let name = title_link
            .find(Text)
            .next()
            .and_then(|el| el.as_text())
            .ok_or(Error::MissingElement(".post-title > a > text"))?
            .to_string();

        let link = MangaUrl::from_url(
            Url::parse(
                title_link
                    .attr("href")
                    .ok_or(Error::MissingElement(".post-title > a[href]"))?,
            )
            .map_err(Error::InvalidHtmlLink)?,
        )
        .map_err(Error::InvalidMangaUrl)?;

        Ok(Self { name, link })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const SEARCH_RESULTS_OBSERVATION: &str =
        include_str!("../../test_data/search_observation.html");
    const SEARCH_RESULTS_INVALID: &str = include_str!("../../test_data/invalid_search.html");

    #[test]
    fn parse_search_results_observation() {
        let doc = Document::from(SEARCH_RESULTS_OBSERVATION);
        let res = SearchResults::from_doc(&doc).unwrap();
        dbg!(&res);
    }

    #[test]
    fn parse_search_results_invalid() {
        let doc = Document::from(SEARCH_RESULTS_INVALID);
        let res = SearchResults::from_doc(&doc).unwrap();
        assert!(res.results.is_empty());
    }
}
