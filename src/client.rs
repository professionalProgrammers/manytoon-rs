use crate::{
    Error,
    Manga,
    MangaPage,
    MangaPageUrl,
    MangaUrl,
    Result,
    SearchResults,
};
use select::document::Document;
use std::io::Write;
use url::Url;

/// A manytoon client
#[derive(Debug)]
pub struct Client {
    client: reqwest::Client,
}

impl Client {
    /// Make a new Client
    pub fn new() -> Self {
        Self {
            client: reqwest::Client::new(),
        }
    }

    /// Get a url as a Document
    pub async fn get_doc(&self, url: &str) -> Result<Document> {
        let res = self.client.get(url).send().await?;

        let status = res.status();
        if !status.is_success() {
            return Err(Error::InvalidStatus(status));
        }

        let text = res.text().await?;
        let doc = Document::from(text.as_str());

        Ok(doc)
    }

    /// Search for a manga
    pub async fn search(&self, query: &str) -> Result<SearchResults> {
        let url = Url::parse_with_params(
            "https://manytoon.com",
            &[("s", query), ("post_type", "wp-manga")],
        )?;
        let doc = self.get_doc(url.as_str()).await?;
        let res = SearchResults::from_doc(&doc)?;
        Ok(res)
    }

    /// Get a manga using a MangaUrl
    pub async fn get_manga(&self, url: &MangaUrl) -> Result<Manga> {
        let doc = self.get_doc(url.as_str()).await?;
        let manga = Manga::from_doc(&doc)?;
        Ok(manga)
    }

    /// Get a manga page from a MangaPageUrl.
    pub async fn get_manga_page(&self, url: &MangaPageUrl) -> Result<MangaPage> {
        let doc = self.get_doc(url.as_str()).await?;
        let manga_page = MangaPage::from_doc(&doc)?;

        Ok(manga_page)
    }

    /// Get a url and copy it to the given writer
    pub async fn copy_res_to<T: Write>(&self, url: &Url, mut writer: T) -> Result<()> {
        let mut res = self.client.get(url.as_str()).send().await?;
        let status = res.status();
        if !status.is_success() {
            return Err(Error::InvalidStatus(status));
        }

        while let Some(chunk) = res.chunk().await? {
            writer.write_all(&chunk)?;
        }

        Ok(())
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}
