/// Manga Types;
pub mod manga;
/// Search Types
pub mod search;

pub use self::{
    manga::{
        Manga,
        MangaChapter,
        MangaPage,
    },
    search::{
        SearchEntry,
        SearchResults,
    },
};
use crate::{
    Error,
    Result,
    COMIC_URL_BASE,
};
use url::Url;

/// A Url for a Manga
#[derive(Debug)]
pub struct MangaUrl(Url);

impl MangaUrl {
    /// Try to make a url from a manga slug
    pub fn from_manga_slug(slug: &str) -> Self {
        let mut url = Url::parse(COMIC_URL_BASE).expect("Valid Base Url");
        url.path_segments_mut()
            .expect("Non cannot-be-a-base COMIC_URL_BASE")
            .push(slug);

        Self(url)
    }

    /// Try to make a MangaUrl from a Url. The error is the original Url.
    pub fn from_url(url: Url) -> std::result::Result<Self, Url> {
        if url.host_str() != Some("manytoon.com") {
            return Err(url);
        }

        let mut iter = match url.path_segments() {
            Some(iter) => iter,
            None => return Err(url),
        };

        let comic_segment = match iter.next() {
            Some(comic_segment) => comic_segment,
            None => return Err(url),
        };

        if "comic" != comic_segment {
            return Err(url);
        }

        let _manga_slug = match iter.next() {
            Some(slug) => slug,
            None => return Err(url),
        };

        for seg in iter {
            if !seg.is_empty() {
                return Err(url);
            }
        }

        Ok(Self(url))
    }

    /// Try to make a MangaUrl form a url str.
    pub fn from_url_str(url: &str) -> Result<Self> {
        Self::from_url(Url::parse(url)?).map_err(Error::InvalidMangaUrl)
    }

    /// Get the manga slug from this MangaUrl
    pub fn manga_slug(&self) -> &str {
        self.0
            .path_segments()
            .expect("Non cannot-be-a-base MangaUrl")
            .nth(1)
            .expect("Valid MangaUrl with manga slug")
    }

    /// Get this MangaUrl as an &Url
    pub fn as_url(&self) -> &Url {
        &self.0
    }

    /// Get this MangaUrl as an &str
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

/// A Url for a Manga Pages
#[derive(Debug)]
pub struct MangaPageUrl(Url);

impl MangaPageUrl {
    /// Try to make a MangaPageUrl from a manga slug and a manga page slug
    pub fn from_slugs(manga_slug: &str, manga_page_slug: &str) -> Self {
        let mut url = Url::parse(COMIC_URL_BASE).expect("Valid Base Url");
        url.path_segments_mut()
            .expect("Non cannot-be-a-base COMIC_URL_BASE")
            .extend(&[manga_slug, manga_page_slug]);

        Self(url)
    }

    /// Try to make a MangaChapterUrl from a Url. The error is the original Url.
    pub fn from_url(url: Url) -> std::result::Result<Self, Url> {
        if url.host_str() != Some("manytoon.com") {
            return Err(url);
        }

        let mut iter = match url.path_segments() {
            Some(iter) => iter,
            None => return Err(url),
        };

        let comic_segment = match iter.next() {
            Some(comic_segment) => comic_segment,
            None => return Err(url),
        };

        if "comic" != comic_segment {
            return Err(url);
        }

        let _manga_slug = match iter.next() {
            Some(slug) => slug,
            None => return Err(url),
        };

        let _manga_chapter_slug = match iter.next() {
            Some(slug) => slug,
            None => return Err(url),
        };

        for seg in iter {
            if !seg.is_empty() {
                return Err(url);
            }
        }

        Ok(Self(url))
    }

    /// Try to make a MangaPageUrl form a url str.
    pub fn from_url_str(url: &str) -> Result<Self> {
        Self::from_url(Url::parse(url)?).map_err(Error::InvalidMangaPageUrl)
    }

    /// Get the manga slug from this MangaPageUrl
    pub fn manga_slug(&self) -> &str {
        self.0
            .path_segments()
            .expect("Non cannot-be-a-base MangaPageUrl")
            .nth(1)
            .expect("Valid MangaPageUrl with manga slug")
    }

    /// Get the manga page slug from this MangaPageUrl
    pub fn manga_page_slug(&self) -> &str {
        self.0
            .path_segments()
            .expect("Non cannot-be-a-base MangaPageUrl")
            .nth(2)
            .expect("Valid MangaPageUrl with manga page slug")
    }

    /// Get this MangaPageUrl as an &Url
    pub fn as_url(&self) -> &Url {
        &self.0
    }

    /// Get this MangaPageUrl as an &str
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}
