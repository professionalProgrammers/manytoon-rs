# manytoon-rs

[![Build status](https://ci.appveyor.com/api/projects/status/60688i5khse52gf4?svg=true)](https://ci.appveyor.com/project/professionalProgrammers/manytoon-rs)
[![](https://tokei.rs/b1/bitbucket.org/professionalProgrammers/manytoon-rs)](https://bitbucket.org/professionalProgrammers/manytoon-rs)

A Rust API for https://manytoon.com.